package com.questionnaire.questionnairandroid

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration

class SpaceOnLastItemDecoration(private val extraBottomSpace: Int) : ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val itemPosition = parent.getChildAdapterPosition(view)
        val itemCount = state.itemCount
        if (itemCount > 0 && itemPosition == itemCount - 1) {
            outRect.bottom = extraBottomSpace
        }
    }

}