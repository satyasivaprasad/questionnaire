package com.questionnaire.questionnairandroid

import android.content.Context
import android.util.Log
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.questionnaire.questionnairandroid.question.Question
import com.questionnaire.questionnairandroid.question.QuestionsWrapper
import com.questionnaire.questionnairandroid.question.QuestionsWrapper.SubmitData
import java.io.IOException
import java.util.*

class Questions(val questions: ArrayList<Question>?, val submitData: SubmitData?) {

    fun getQuestionFor(position: Int): Question? {
        return if (position >= questions!!.size) {
            null
        } else questions[position]
    }

    fun size(): Int {
        return questions!!.size
    }

    companion object {
        private val TAG = Questions::class.java.simpleName
        @JvmStatic
        fun load(context: Context, jsonFileName: String): Questions {
            return createSurveyQuestionsFromFile(context, jsonFileName)
        }

        fun createSurveyQuestionsFromFile(context: Context, jsonFileName: String): Questions {
            var questions = ArrayList<Question>()
            var submitData: SubmitData? = null
            val assetManager = context.assets
            val mapper = ObjectMapper()
                    .enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
                    .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            try {
                val inputStream = assetManager.open(jsonFileName)
                val wrapper = mapper.readValue<QuestionsWrapper>(inputStream, object : TypeReference<QuestionsWrapper?>() {})
                if (wrapper.questions != null) {
                    questions = wrapper.questions!!
                }
                submitData = wrapper.submit
                inputStream.close()
            } catch (ioe: IOException) {
                Log.e(TAG, "Error while parsing $jsonFileName", ioe)
            }
            return Questions(questions, submitData)
        }

        fun createSurveyQuestionsFromJsonString(context: Context?, jsonString: String): Questions {
            var questions = ArrayList<Question>()
            var submitData: SubmitData? = null
            val mapper = ObjectMapper()
                    .enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
                    .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            try {
                val wrapper = mapper.readValue<QuestionsWrapper>(jsonString, object : TypeReference<QuestionsWrapper?>() {})
                if (wrapper.questions != null) {
                    questions = wrapper.questions!!
                }
                submitData = wrapper.submit
            } catch (ioe: IOException) {
                Log.e(TAG, "Error while parsing jsonString: $jsonString", ioe)
            }
            return Questions(questions, submitData)
        }
    }

}