package com.questionnaire.questionnairandroid

import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.questionnaire.questionnairandroid.Questions.Companion.load
import com.questionnaire.questionnairandroid.condition.CustomConditionHandler
import com.questionnaire.questionnairandroid.validation.DefaultValidator
import com.questionnaire.questionnairandroid.validation.FailedValidationListener
import com.questionnaire.questionnairandroid.validation.Validator

abstract class QuestionnaireActivity : AppCompatActivity(), FailedValidationListener {
    private var mState: QuestionnaireState? = null
    private var mOnSurveyStateChangedListener: OnSurveyStateChangedListener? = null
    var recyclerView: RecyclerView? = null
        private set
    private var rockBtn: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResId)
        title = surveyTitle
        val questions = createSurveyQuestions()
        Log.d(TAG, "Questions = $questions")
        mState = createSurveyState(questions)
        setupRecyclerView()
        rockBtn = findViewById(R.id.letsRockBtn)
        rockBtn!!.setOnClickListener(View.OnClickListener { //                Log.v(LOG_TAG, "Demo "  +mState.getmQuestionStateMap());
            for (question in questions.questions!!) {
                val answer = mState!!.getStateFor(question.id)!!.answer
                if (answer != null) {
                    Log.v(LOG_TAG, "Demo State " + answer.value)
                    Log.v(LOG_TAG, "Demo State " + answer.valueList)
                    Log.v(LOG_TAG, "Demo State " + answer.valueMap)
                }
            }
        })
    }

    @get:LayoutRes
    protected val layoutResId: Int
        protected get() = R.layout.activity_survey

    protected fun createSurveyQuestions(): Questions {
        return load(this, jsonFilename!!)
    }

    protected fun setupRecyclerView() {
        recyclerView = findViewById(R.id.recycler_view)
        recyclerView!!.setLayoutManager(LinearLayoutManager(this))
        //        mRecyclerView.addItemDecoration(new SpaceOnLastItemDecoration(getDisplayHeightPixels()));
        recyclerView!!.setItemAnimator(QuestionnaireItemAnimator())
        val adapter = QuestionnaireAdapter(this, mState!!)
        recyclerView!!.setAdapter(adapter)
        Log.v(LOG_TAG, "Demo " + mState!!.getQuestionStateMap())
    }

    protected fun createSurveyState(questions: Questions?): QuestionnaireState {
        return QuestionnaireState(questions!!)
                .setValidator(validator)
                .setCustomConditionHandler(customConditionHandler)
                .setSubmitSurveyHandler(submitSurveyHandler)
                .initFilter()
    }

    private val displayHeightPixels: Int
        private get() {
            val displayMetrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(displayMetrics)
            return displayMetrics.heightPixels
        }

    override fun onStart() {
        super.onStart()
        mState!!.addOnSurveyStateChangedListener(onSurveyStateChangedListener!!)
    }

    override fun onStop() {
        super.onStop()
        mState!!.removeOnSurveyStateChangedListener(onSurveyStateChangedListener!!)
    }

    protected open val surveyTitle: String?
        protected get() = if (intent.hasExtra(SURVEY_TITLE_EXTRA)) {
            intent.getStringExtra(SURVEY_TITLE_EXTRA)
        } else null

    protected open val jsonFilename: String?
        protected get() = if (intent.hasExtra(JSON_FILE_NAME_EXTRA)) {
            intent.getStringExtra(JSON_FILE_NAME_EXTRA)
        } else null

    protected val validator: Validator
        protected get() = DefaultValidator(this)

    // Subclasses should return a non-null if they are using custom show_if conditions.
    protected open val customConditionHandler: CustomConditionHandler?
        protected get() = null

    protected val answerProvider: AnswerProvider?
        protected get() = mState

    override fun validationFailed(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    val submitSurveyHandler: SubmitQuestionnaireHandler
        get() = DefaultSubmitQuestionnaireHandler(this)

    var onSurveyStateChangedListener: OnSurveyStateChangedListener?
        get() {
            if (mOnSurveyStateChangedListener == null) {
                mOnSurveyStateChangedListener = DefaultOnSurveyStateChangedListener(this, recyclerView!!)
            }
            return mOnSurveyStateChangedListener
        }
        set(listener) {
            mOnSurveyStateChangedListener = listener
        }

    companion object {
        private val TAG = QuestionnaireActivity::class.java.simpleName
        const val JSON_FILE_NAME_EXTRA = "json_filename"
        const val SURVEY_TITLE_EXTRA = "survey_title"
        private val LOG_TAG = QuestionnaireActivity::class.java.simpleName
    }
}