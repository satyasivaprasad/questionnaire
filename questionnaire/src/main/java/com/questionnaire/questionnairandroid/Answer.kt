package com.questionnaire.questionnairandroid

import java.util.*

// An answer can be a string, a list of strings, or a map with String keys.
// Only one of these values should be set
class Answer {
    var value: String? = null
        private set
    var valueList: ArrayList<String>? = null
        private set
    var valueMap: HashMap<String, Answer>? = null
        private set

    constructor(value: String) {
        this.value = value
    }

    constructor(valueList: ArrayList<String>) {
        this.valueList = valueList
    }

    constructor(valueMap: HashMap<String, Answer>) {
        this.valueMap = valueMap
    }

    val isString: Boolean
        get() = value != null

    val isList: Boolean
        get() = valueList != null

    val isMap: Boolean
        get() = valueMap != null

}