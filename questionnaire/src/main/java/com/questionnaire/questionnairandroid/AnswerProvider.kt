package com.questionnaire.questionnairandroid

interface AnswerProvider {
    fun answerFor(questionId: String): Answer
    fun allAnswersJson(): String?
}