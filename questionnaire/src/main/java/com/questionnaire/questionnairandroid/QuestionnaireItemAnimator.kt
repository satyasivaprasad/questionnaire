package com.questionnaire.questionnairandroid

import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView

// Slides new questions in from the right.
class QuestionnaireItemAnimator : DefaultItemAnimator() {
    override fun animateAppearance(viewHolder: RecyclerView.ViewHolder, preLayoutInfo: ItemHolderInfo?, postLayoutInfo: ItemHolderInfo): Boolean {
        var preLayoutInfo = preLayoutInfo
        if (preLayoutInfo == null) {
            preLayoutInfo = ItemHolderInfo()
            preLayoutInfo.setFrom(viewHolder)
        }
        preLayoutInfo.left = postLayoutInfo.right
        return animateMove(viewHolder, preLayoutInfo.left, preLayoutInfo.top, postLayoutInfo.left, postLayoutInfo.top)
    }
}