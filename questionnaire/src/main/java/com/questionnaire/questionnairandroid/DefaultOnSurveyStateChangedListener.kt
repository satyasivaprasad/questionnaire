package com.questionnaire.questionnairandroid

import android.content.Context
import android.util.DisplayMetrics
import android.util.Log
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.questionnaire.questionnairandroid.util.KeyboardUtil.hideKeyboard

class DefaultOnSurveyStateChangedListener(private val context: Context, private val recyclerView: RecyclerView) : OnSurveyStateChangedListener {
    private val smoothScroller: LinearSmoothScroller

    fun getAdapter() = recyclerView.adapter!!

    private val layoutManager: RecyclerView.LayoutManager?
        private get() = recyclerView.layoutManager

    override fun questionInserted(adapterPosition: Int) {
        if (getAdapter() == null) {
            Log.e(TAG, "Adapter is null during questionInserted")
            return
        }
        if (adapterPosition > 0) {
            // This fixes the ItemDecoration "footer"
            recyclerView.adapter?.notifyItemChanged(adapterPosition - 1)
        }
        if (adapterPosition < getAdapter().getItemCount() - 1) {
            // Only animate if this is a brand-new question
            getAdapter().notifyItemChanged(adapterPosition)
            return
        }
        getAdapter().notifyItemInserted(adapterPosition)
        smoothScroller.targetPosition = adapterPosition
        hideKeyboard(context, recyclerView)
        // This ensures that the keyboard finishes hiding before we start scrolling
        recyclerView.post {
            if (layoutManager != null) {
                layoutManager!!.startSmoothScroll(smoothScroller)
            }
        }
    }

    override fun questionRemoved(adapterPosition: Int) {
        if (getAdapter() == null) {
            Log.e(TAG, "Adapter is null during questionRemoved")
            return
        }
        if (adapterPosition > 0) {
            // This fixes the ItemDecoration "footer"
            getAdapter().notifyItemChanged(adapterPosition - 1)
        }
        getAdapter().notifyItemRemoved(adapterPosition)
    }

    override fun questionsRemoved(startAdapterPosition: Int, itemCount: Int) {
        if (getAdapter() == null) {
            Log.e(TAG, "Adapter is null during questionRemoved")
            return
        }
        if (startAdapterPosition > 0) {
            // This fixes the ItemDecoration "footer"
            getAdapter().notifyItemChanged(startAdapterPosition - 1)
        }
        getAdapter().notifyItemRangeRemoved(startAdapterPosition, itemCount)
    }

    override fun questionChanged(adapterPosition: Int) {
        if (getAdapter() == null) {
            Log.e(TAG, "Adapter is null during questionChanged")
            return
        }
        getAdapter().notifyItemChanged(adapterPosition)
    }

    override fun submitButtonInserted(adapterPosition: Int) {
        if (getAdapter() == null) {
            Log.e(TAG, "Adapter is null during submitButtonInserted")
            return
        }
        if (adapterPosition > 0) {
            // This fixes the ItemDecoration "footer"
            getAdapter().notifyItemChanged(adapterPosition - 1)
        }
        getAdapter().notifyItemInserted(adapterPosition)
    }

    companion object {
        private val TAG = DefaultOnSurveyStateChangedListener::class.java.simpleName
    }

    init {
        smoothScroller = object : LinearSmoothScroller(context) {
            override fun getVerticalSnapPreference(): Int {
                return SNAP_TO_START
            }

            override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics): Float {
                return 100f / displayMetrics.densityDpi.toFloat()
            }
        }
    }
}