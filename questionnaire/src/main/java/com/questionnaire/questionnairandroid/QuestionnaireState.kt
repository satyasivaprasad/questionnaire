package com.questionnaire.questionnairandroid

import android.util.Log
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.questionnaire.questionnairandroid.condition.ConditionEvaluator
import com.questionnaire.questionnairandroid.condition.CustomConditionHandler
import com.questionnaire.questionnairandroid.condition.FilteredQuestions
import com.questionnaire.questionnairandroid.condition.FilteredQuestions.QuestionAdapterPosition
import com.questionnaire.questionnairandroid.condition.OnQuestionSkipStatusChangedListener
import com.questionnaire.questionnairandroid.question.Question
import com.questionnaire.questionnairandroid.question.QuestionsWrapper
import com.questionnaire.questionnairandroid.validation.Validator
import java.util.*

// Tracks the current state of our Questionnaire.
class QuestionnaireState(private val questions: Questions) : OnQuestionStateChangedListener, AnswerProvider {
    fun getQuestionStateMap(): Map<String?, QuestionState> {
        return questionStateMap
    }

    private val questionStateMap: MutableMap<String?, QuestionState>
    internal var validator: Validator? = null
    val conditionEvaluator: ConditionEvaluator
    private var filteredQuestions: FilteredQuestions? = null
    var submitQuestionnaireHandler: SubmitQuestionnaireHandler? = null
        private set
    private val onSurveyStateChangedListeners: MutableList<OnSurveyStateChangedListener> = ArrayList()
    private val onQuestionStateChangedListeners: MutableList<OnQuestionStateChangedListener> = ArrayList()
    private var visibleQuestionCount = 1
    var isSubmitButtonShown = false
        private set
    private var mObjectMapper: ObjectMapper? = null
    fun setValidator(validator: Validator?): QuestionnaireState {
        this.validator = validator
        return this
    }

    fun setCustomConditionHandler(handler: CustomConditionHandler?): QuestionnaireState {
        conditionEvaluator.setCustomConditionHandler(handler)
        return this
    }

    fun setSubmitSurveyHandler(submitQuestionnaireHandler: SubmitQuestionnaireHandler?): QuestionnaireState {
        this.submitQuestionnaireHandler = submitQuestionnaireHandler
        return this
    }

    fun getVisibleQuestionCount(): Int {
        return questions.size()
    }

    fun initFilter(): QuestionnaireState {
        filteredQuestions = FilteredQuestions(questions, conditionEvaluator)
        filteredQuestions!!.setOnQuestionSkipStatusChangedListener(object : OnQuestionSkipStatusChangedListener {
            override fun skipStatusChanged(newlySkippedQuestionIds: Set<QuestionAdapterPosition?>?, newlyShownQuestionIds: Set<QuestionAdapterPosition?>?) {
                val removedPositions: MutableSet<Int> = HashSet()
                val addedPositions: MutableSet<Int> = HashSet()
                for (skippedQ in newlySkippedQuestionIds!!) {
                    removedPositions.add(skippedQ!!.adapterPosition)
                }
                for (shownQ in newlyShownQuestionIds!!) {
                    addedPositions.add(shownQ!!.adapterPosition)
                }
                // Always remove the submit button, if present.  It will get added later if necessary
                if (isSubmitButtonShown) {
                    isSubmitButtonShown = false
                    questionRemoved(filteredQuestions!!.size())
                }
                val visibleQuestionCount = visibleQuestionCount
                for (i in 0 until visibleQuestionCount) {
                    val addedId = getIdByPosition(newlyShownQuestionIds, i)
                    if (removedPositions.contains(i) && addedPositions.contains(i)) {
                        questionChanged(i)
                    } else if (addedPositions.contains(i)) {
                        this@QuestionnaireState.visibleQuestionCount++
                        questionInserted(i)
                    } else if (removedPositions.contains(i)) {
                        this@QuestionnaireState.visibleQuestionCount--
                        questionRemoved(i)
                    }
                    if (addedId != null && !isQuestionAnswered(addedId)) {
                        this@QuestionnaireState.visibleQuestionCount = i + 1
                        questionsRemoved(visibleQuestionCount, visibleQuestionCount - i)
                        break
                    }
                }
            }

            private fun getIdByPosition(questionPositions: Set<QuestionAdapterPosition?>?, position: Int): String? {
                for (questionAdapterPosition in questionPositions!!) {
                    if (questionAdapterPosition!!.adapterPosition == position) {
                        return questionAdapterPosition.questionId
                    }
                }
                return null
            }
        })
        return this
    }

    fun isQuestionAnswered(questionId: String?): Boolean {
        return getStateFor(questionId)!!.isAnswered
    }

    fun getQuestionFor(adapterPosition: Int): Question? {
        checkNotNull(filteredQuestions) { "Please call initFilter on SurveyState!" }
        return if (isSubmitPosition(adapterPosition)) {
            null
        } else filteredQuestions!!.getQuestionFor(adapterPosition)
    }

    fun isSubmitPosition(adapterPosition: Int): Boolean {
        return filteredQuestions!!.size() == adapterPosition
    }

    fun getStateFor(questionId: String?): QuestionState? {
        return if (questionStateMap.containsKey(questionId)) {
            questionStateMap[questionId]
        } else {
            val questionState = QuestionState(questionId!!, this)
            questionStateMap[questionId] = questionState
            questionState
        }
    }

    override fun questionStateChanged(newQuestionState: QuestionState) {
        questionStateMap[newQuestionState.id()] = newQuestionState
        for (listener in onQuestionStateChangedListeners) {
            listener.questionStateChanged(newQuestionState)
        }
    }

    override fun questionAnswered(newQuestionState: QuestionState) {
        questionStateMap[newQuestionState.id()] = newQuestionState
        filteredQuestions!!.questionAnswered(newQuestionState)
        var lastQuestion = getQuestionFor(visibleQuestionCount - 1)
        while (!isSubmitButtonShown && lastQuestion != null && isAnswered(lastQuestion)) {
            increaseVisibleQuestionCount()
            lastQuestion = getQuestionFor(visibleQuestionCount - 1)
        }
        for (listener in onQuestionStateChangedListeners) {
            listener.questionAnswered(newQuestionState)
        }
    }

    fun isAnswered(question: Question): Boolean {
        val questionState = getStateFor(question.id)
        return questionState!!.isAnswered
    }

    override fun answerFor(questionId: String): Answer {
        val questionState = getStateFor(questionId)
        return questionState!!.answer!!
    }

    override fun allAnswersJson(): String? {
        val topNode = objectMapper.createObjectNode()
        var answersNode = objectMapper.createObjectNode()
        for (questionId in questionStateMap.keys) {
            answersNode = putAnswer(answersNode, questionId!!, answerFor(questionId))
        }
        topNode["answers"] = answersNode
        return topNode.toString()
    }

    private val objectMapper: ObjectMapper
        private get() {
            if (mObjectMapper == null) {
                mObjectMapper = ObjectMapper()
            }
            return mObjectMapper!!
        }

    private fun putAnswer(objectNode: ObjectNode, key: String, answer: Answer?): ObjectNode {
        if (answer == null) {
            Log.e(TAG, "Answer is null for key: $key")
            return objectNode
        }
        if (answer.isString) {
            objectNode.put(key, answer.value)
        } else if (answer.isList) {
            val answersList = answer.valueList
            val answerListNode = objectNode.putArray(key)
            for (answerStr in answersList!!) {
                answerListNode.add(answerStr)
            }
        } else {
            for (valueKey in answer.valueMap!!.keys) {
                var valueObject = objectMapper.createObjectNode()
                valueObject = putAnswer(valueObject, valueKey, answer.valueMap!![valueKey])
                objectNode[key] = valueObject
            }
        }
        return objectNode
    }

    val submitData: QuestionsWrapper.SubmitData?
        get() = questions.submitData

    fun increaseVisibleQuestionCount() {
        if (visibleQuestionCount < filteredQuestions!!.size()) {
            visibleQuestionCount += 1
            questionInserted(visibleQuestionCount - 1)
        } else if (visibleQuestionCount == filteredQuestions!!.size()) {
            isSubmitButtonShown = true
            submitButtonInserted(visibleQuestionCount)
        }
    }

    fun addOnSurveyStateChangedListener(listener: OnSurveyStateChangedListener) {
        onSurveyStateChangedListeners.add(listener)
    }

    fun removeOnSurveyStateChangedListener(listener: OnSurveyStateChangedListener) {
        onSurveyStateChangedListeners.remove(listener)
    }

    fun addOnQuestionStateChangedListener(listener: OnQuestionStateChangedListener) {
        onQuestionStateChangedListeners.add(listener)
    }

    fun removeOnQuestionStateChangedListener(listener: OnQuestionStateChangedListener) {
        onQuestionStateChangedListeners.remove(listener)
    }

    private fun questionInserted(adapterPosition: Int) {
        for (listener in onSurveyStateChangedListeners) {
            listener.questionInserted(adapterPosition)
        }
    }

    fun questionRemoved(adapterPosition: Int) {
        for (listener in onSurveyStateChangedListeners) {
            listener.questionRemoved(adapterPosition)
        }
    }

    fun questionsRemoved(startAdapterPosition: Int, itemCount: Int) {
        for (listener in onSurveyStateChangedListeners) {
            listener.questionsRemoved(startAdapterPosition, itemCount)
        }
    }

    fun questionChanged(adapterPosition: Int) {
        for (listener in onSurveyStateChangedListeners) {
            listener.questionChanged(adapterPosition)
        }
    }

    fun submitButtonInserted(adapterPosition: Int) {
        for (listener in onSurveyStateChangedListeners) {
            listener.submitButtonInserted(adapterPosition)
        }
    }

    companion object {
        private val TAG = QuestionnaireState::class.java.simpleName
    }

    init {
        questionStateMap = HashMap()
        conditionEvaluator = ConditionEvaluator(this)
    }
}