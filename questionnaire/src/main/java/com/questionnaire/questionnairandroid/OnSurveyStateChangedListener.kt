package com.questionnaire.questionnairandroid

interface OnSurveyStateChangedListener {
    fun questionInserted(adapterPosition: Int)
    fun questionRemoved(adapterPosition: Int)
    fun questionsRemoved(adapterPosition: Int, itemCount: Int)
    fun questionChanged(adapterPosition: Int)
    fun submitButtonInserted(adapterPosition: Int)
}