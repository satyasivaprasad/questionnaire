package com.questionnaire.questionnairandroid.question

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.questionnaire.questionnairandroid.condition.Condition
import java.util.*

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "question_type", visible = true, defaultImpl = UnsupportedQuestion::class)
@JsonSubTypes(JsonSubTypes.Type(value = SingleSelectQuestion::class, name = "single_select"), JsonSubTypes.Type(value = MultiSelectQuestion::class, name = "multi_select"), JsonSubTypes.Type(value = YearPickerQuestion::class, name = "year_picker"), JsonSubTypes.Type(value = DatePickerQuestion::class, name = "date_picker"), JsonSubTypes.Type(value = SingleTextFieldQuestion::class, name = "single_text_field"), JsonSubTypes.Type(value = SingleTextAreaQuestion::class, name = "single_text_area"), JsonSubTypes.Type(value = MultiTextFieldQuestion::class, name = "multi_text_field"), JsonSubTypes.Type(value = DynamicLabelTextFieldQuestion::class, name = "dynamic_label_text_field"), JsonSubTypes.Type(value = AddTextFieldQuestion::class, name = "add_text_field"), JsonSubTypes.Type(value = SegmentSelectQuestion::class, name = "segment_select"), JsonSubTypes.Type(value = TableSelectQuestion::class, name = "table_select"))
abstract class Question {
    @JvmField
    var id: String? = null
    @JvmField
    var header: String? = null
    @JvmField
    var question: String? = null

    @JsonProperty("required")
    var required = false

    @JvmField
    @JsonProperty("question_type")
    var questionType: String? = null

    @JsonProperty("sub_questions")
    var subQuestions: ArrayList<Question>? = null

    @JsonProperty("show_if")
    var showIf: Condition? = null
}