package com.questionnaire.questionnairandroid.question

import com.fasterxml.jackson.annotation.JsonProperty

class AddTextFieldQuestion : Question() {
    @JsonProperty("input_type")
    var inputType: String? = null
}