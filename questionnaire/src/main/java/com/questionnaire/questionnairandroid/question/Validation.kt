package com.questionnaire.questionnairandroid.question

import com.fasterxml.jackson.annotation.JsonProperty

class Validation {
    @JvmField
    var operation: String? = null
    @JvmField
    var value: Double? = null

    @JvmField
    @JsonProperty("answer_to_question_id")
    var answerToQuestionId: String? = null

    @JvmField
    @JsonProperty("on_fail_message")
    var onFailMessage: String? = null

    @JvmField
    @JsonProperty("for_label")
    var forLabel: String? = null
}