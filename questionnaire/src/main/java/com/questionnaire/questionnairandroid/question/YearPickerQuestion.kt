package com.questionnaire.questionnairandroid.question

import com.fasterxml.jackson.annotation.JsonProperty

class YearPickerQuestion : Question() {
    @JsonProperty("min_year")
    var minYear: String? = null

    @JsonProperty("max_year")
    var maxYear: String? = null

    @JsonProperty("num_years")
    var numYears: String? = null

    @JsonProperty("initial_year")
    var initialYear: String? = null

    @JsonProperty("sort_order")
    var sortOrder: String? = null
}