package com.questionnaire.questionnairandroid.question

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

class SingleTextFieldQuestion : Question() {
    @JvmField
    var label: String? = null

    @JvmField
    @JsonProperty("input_type")
    var input_type: String? = null

    @JvmField
    @JsonProperty("max_chars")
    var maxChars: String? = null
    @JvmField
    var validations: ArrayList<Validation>? = null
}