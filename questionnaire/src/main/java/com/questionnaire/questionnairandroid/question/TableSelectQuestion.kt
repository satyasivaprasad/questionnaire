package com.questionnaire.questionnairandroid.question

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

class TableSelectQuestion : Question() {
    var options: ArrayList<Option>? = null

    @JsonProperty("table_questions")
    var tableQuestions: ArrayList<TableQuestion>? = null

    class TableQuestion {
        var id: String? = null
        var title: String? = null
    }
}