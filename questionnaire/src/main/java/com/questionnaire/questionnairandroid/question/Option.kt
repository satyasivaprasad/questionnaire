package com.questionnaire.questionnairandroid.question

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.questionnaire.questionnairandroid.question.Option.OptionDeserializer
import java.io.IOException

@JsonDeserialize(using = OptionDeserializer::class)
open class Option {
    @JvmField
    var title: String? = null

    class OptionDeserializer protected constructor() : StdDeserializer<Option>(Option::class.java) {
        @Throws(IOException::class, JsonProcessingException::class)
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Option {
            val node = p.codec.readTree<JsonNode>(p)
            return if (node.has("title")) {
                val otherOption = OtherOption()
                otherOption.title = node["title"].asText()
                otherOption.type = node["type"].asText()
                otherOption
            } else {
                val option = Option()
                option.title = node.asText()
                option
            }
        }
    }
}