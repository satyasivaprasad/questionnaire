package com.questionnaire.questionnairandroid.question

import com.fasterxml.jackson.annotation.JsonProperty

class DatePickerQuestion : Question() {
    var date: String? = null

    @JsonProperty("min_date")
    var minDate: String? = null

    @JsonProperty("max_date")
    var maxDate: String? = null

    @JsonProperty("date_diff")
    var dateDiff: DateDiff? = null

    class DateDiff {
        var year: Int? = null
        var month: Int? = null
        var day: Int? = null
    }
}