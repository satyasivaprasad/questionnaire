package com.questionnaire.questionnairandroid.question

class OtherOption : Option() {
    @JvmField
    var type: String? = null
}