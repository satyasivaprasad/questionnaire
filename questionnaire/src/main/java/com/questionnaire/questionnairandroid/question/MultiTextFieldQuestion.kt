package com.questionnaire.questionnairandroid.question

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

class MultiTextFieldQuestion : Question() {
    @JsonProperty("max_chars")
    var maxChars: String? = null
    var fields: ArrayList<Field>? = null

    class Field {
        var label: String? = null

        @JsonProperty("input_type")
        var inputType: String? = null
    }
}