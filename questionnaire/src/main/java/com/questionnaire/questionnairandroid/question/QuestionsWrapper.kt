package com.questionnaire.questionnairandroid.question

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
class QuestionsWrapper {
    @JvmField
    var questions: ArrayList<Question>? = null
    @JvmField
    var submit: SubmitData? = null

    class SubmitData {
        @JvmField
        @JsonProperty("button_title")
        var buttonTitle: String? = null
        @JvmField
        var url: String? = null
    }
}