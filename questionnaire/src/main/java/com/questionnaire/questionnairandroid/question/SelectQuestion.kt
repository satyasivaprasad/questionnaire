package com.questionnaire.questionnairandroid.question

import java.util.*

abstract class SelectQuestion : Question() {
    @JvmField
    var options: ArrayList<Option>? = null
}