package com.questionnaire.questionnairandroid.question

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

class DynamicLabelTextFieldQuestion : Question() {
    @JsonProperty("label_options")
    var labelOptions: ArrayList<ArrayList<String>>? = null

    @JsonProperty("input_type")
    var inputType: String? = null
    var validations: ArrayList<Validation>? = null
}