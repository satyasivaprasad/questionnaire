package com.questionnaire.questionnairandroid.question

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

class SingleTextAreaQuestion : Question() {
    @JvmField
    @JsonProperty("max_chars")
    var maxChars: String? = null
    @JvmField
    var validations: ArrayList<Validation>? = null
}