package com.questionnaire.questionnairandroid.question

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

class SegmentSelectQuestion : Question() {
    @JvmField
    var values: ArrayList<String>? = null

    @JvmField
    @JsonProperty("low_tag")
    var lowTag: String? = null

    @JvmField
    @JsonProperty("high_tag")
    var highTag: String? = null
}