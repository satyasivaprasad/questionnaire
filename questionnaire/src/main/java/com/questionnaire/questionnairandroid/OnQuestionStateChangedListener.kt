package com.questionnaire.questionnairandroid

interface OnQuestionStateChangedListener {
    fun questionStateChanged(newQuestionState: QuestionState)
    fun questionAnswered(newQuestionState: QuestionState)
}