package com.questionnaire.questionnairandroid

import java.util.*

class QuestionState internal constructor(questionId: String, listener: OnQuestionStateChangedListener?) {
    private val questionStringData: MutableMap<String, String>
    private val questionStringListData: MutableMap<String, ArrayList<String>>
    private var mAnswer: Answer? = null
    private val onQuestionStateChangedListener: OnQuestionStateChangedListener?
    fun put(key: String, value: String) {
        require(key != QUESTION_ID_KEY) { "The QuestionId cannot be updated!" }
        questionStringData[key] = value
        onQuestionStateChangedListener?.questionStateChanged(this)
    }

    fun put(key: String, value: Boolean) {
        questionStringData[key] = value.toString()
    }

    fun put(key: String, value: ArrayList<String>) {
        questionStringListData[key] = value
    }

    fun addStringToList(key: String, value: String) {
        if (questionStringListData.containsKey(key)) {
            questionStringListData[key]!!.add(value)
        } else {
            val newList = ArrayList<String>()
            newList.add(value)
            questionStringListData[key] = newList
        }
    }

    fun removeStringFromList(key: String, value: String) {
        if (questionStringListData.containsKey(key)) {
            questionStringListData[key]!!.remove(value)
        }
    }

    private fun containsKey(key: String): Boolean {
        return questionStringData.containsKey(key) || questionStringListData.containsKey(key)
    }

    fun getString(key: String): String? {
        return questionStringData[key]
    }

    fun getString(key: String, defaultValue: String?): String? {
        return if (containsKey(key)) getString(key) else defaultValue
    }

    fun getBool(key: String, defaultValue: Boolean): Boolean {
        return if (containsKey(key)) java.lang.Boolean.valueOf(getString(key)) else defaultValue
    }

    fun getList(key: String): ArrayList<String> {
        return questionStringListData[key]!!
    }

    fun getList(key: String, defaultValue: ArrayList<String?>?): ArrayList<out String?> {
        return (if (questionStringListData.containsKey(key)) questionStringListData[key] else defaultValue)!!
    }

    var answer: Answer?
        get() = mAnswer
        set(answer) {
            mAnswer = answer
            onQuestionStateChangedListener?.questionAnswered(this)
        }

    val isAnswered: Boolean
        get() = mAnswer != null

    fun id(): String? {
        return questionStringData[QUESTION_ID_KEY]
    }

    companion object {
        private const val QUESTION_ID_KEY = "question_id"
    }

    init {
        questionStringData = HashMap()
        questionStringData[QUESTION_ID_KEY] = questionId
        questionStringListData = HashMap()
        onQuestionStateChangedListener = listener
    }
}