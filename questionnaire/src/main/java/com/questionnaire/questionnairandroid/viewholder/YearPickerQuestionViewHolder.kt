package com.questionnaire.questionnairandroid.viewholder

import android.content.Context
import android.view.View
import com.questionnaire.questionnairandroid.QuestionState
import com.questionnaire.questionnairandroid.question.YearPickerQuestion

class YearPickerQuestionViewHolder(context: Context?, itemView: View) : QuestionViewHolder<YearPickerQuestion?>(context!!, itemView) {
    fun bind(question: YearPickerQuestion?, questionState: QuestionState?) {
        super.bind(question)
        // TODO
    }
}