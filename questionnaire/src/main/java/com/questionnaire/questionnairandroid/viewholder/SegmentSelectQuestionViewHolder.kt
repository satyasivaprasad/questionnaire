package com.questionnaire.questionnairandroid.viewholder

import android.content.Context
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import androidx.annotation.AttrRes
import com.questionnaire.questionnairandroid.Answer
import com.questionnaire.questionnairandroid.QuestionState
import com.questionnaire.questionnairandroid.R
import com.questionnaire.questionnairandroid.question.SegmentSelectQuestion

class SegmentSelectQuestionViewHolder(context: Context?, itemView: View) : QuestionViewHolder<SegmentSelectQuestion?>(context!!, itemView) {
    private val segmentSelector: RadioGroup
    private val tagContainer: ViewGroup
    private val lowTagText: TextView
    private val highTagText: TextView
    fun bind(question: SegmentSelectQuestion, questionState: QuestionState) {
        super.bind(question)
        val layoutInflater = LayoutInflater.from(context)
        var checkedId = -1
        val selectedSegment = questionState.getString(SELECTED_SEGMENT_KEY)
        for (i in question.values!!.indices) {
            val radioButton = layoutInflater.inflate(R.layout.radio_button_segment, segmentSelector, false) as RadioButton
            @AttrRes var backgroundAttr: Int
            backgroundAttr = if (i == 0) {
                R.attr.firstSegmentBackground
            } else if (i + 1 == question.values!!.size) {
                R.attr.lastSegmentBackground
            } else {
                R.attr.middleSegmentBackground
            }
            val typedValue = TypedValue()
            val theme = context.theme
            theme.resolveAttribute(backgroundAttr, typedValue, true)
            radioButton.setBackgroundResource(typedValue.resourceId)
            radioButton.text = question.values!![i]
            segmentSelector.addView(radioButton)
            if (question.values!![i] == selectedSegment) {
                checkedId = radioButton.id
            }
        }
        segmentSelector.check(checkedId)
        segmentSelector.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            if (checkedId == -1) {
                return@OnCheckedChangeListener
            }
            val selectedButton = segmentSelector.findViewById<RadioButton>(checkedId)
            val selectedTitle = selectedButton.text.toString()
            questionState.put(SELECTED_SEGMENT_KEY, selectedTitle)
            questionState.answer = Answer(selectedTitle)
        })
        tagContainer.visibility = if (question.lowTag != null || question.highTag != null) View.VISIBLE else View.GONE
        lowTagText.text = question.lowTag
        highTagText.text = question.highTag
    }

    override fun resetState() {
        super.resetState()
        segmentSelector.setOnCheckedChangeListener(null)
        segmentSelector.removeAllViews()
    }

    companion object {
        private const val SELECTED_SEGMENT_KEY = "selected_segment"
    }

    init {
        segmentSelector = itemView.findViewById(R.id.segment_selector)
        tagContainer = itemView.findViewById(R.id.tag_container)
        lowTagText = itemView.findViewById(R.id.low_tag)
        highTagText = itemView.findViewById(R.id.high_tag)
    }
}