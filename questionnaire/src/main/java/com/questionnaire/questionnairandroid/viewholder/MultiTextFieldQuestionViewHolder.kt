package com.questionnaire.questionnairandroid.viewholder

import android.content.Context
import android.view.View
import com.questionnaire.questionnairandroid.QuestionState
import com.questionnaire.questionnairandroid.question.MultiTextFieldQuestion

class MultiTextFieldQuestionViewHolder(context: Context, itemView: View) : QuestionViewHolder<MultiTextFieldQuestion?>(context, itemView) {
    fun bind(question: MultiTextFieldQuestion?, questionState: QuestionState?) {
        super.bind(question)
        // TODO
    }
}