package com.questionnaire.questionnairandroid.viewholder

import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView.OnEditorActionListener
import com.questionnaire.questionnairandroid.Answer
import com.questionnaire.questionnairandroid.QuestionState
import com.questionnaire.questionnairandroid.R
import com.questionnaire.questionnairandroid.question.MultiSelectQuestion
import com.questionnaire.questionnairandroid.question.OtherOption
import com.questionnaire.questionnairandroid.util.KeyboardUtil.hideKeyboard
import com.questionnaire.questionnairandroid.util.KeyboardUtil.showKeyboard
import com.questionnaire.questionnairandroid.util.SimpleTextWatcher
import java.util.*

class MultiSelectQuestionViewHolder(context: Context, itemView: View) : QuestionViewHolder<MultiSelectQuestion?>(context, itemView) {
    private val answerCheckboxContainer: ViewGroup
    private val nextButton: Button
    private val otherMap: MutableMap<String, OtherView> = HashMap()
    var editTextWatcher: TextWatcher? = null
    fun bind(question: MultiSelectQuestion, questionState: QuestionState) {
        super.bind(question)
        val layoutInflater = LayoutInflater.from(context)
        val checkedTitles = questionState.getList(CHECKED_TITLES_KEY, ArrayList())
        for (option in question.options!!) {
            val checkBox = CheckBox(context)
            checkBox.text = option.title
            checkBox.isChecked = checkedTitles.contains(option.title)
            editTextWatcher = object : SimpleTextWatcher() {
                override fun afterTextChanged(s: Editable) {
                    questionState.put(getEditTextKey(option.title), s.toString())
                    if (hasBeenAnswered(questionState)) {
                        onNext(questionState)
                    }
                }
            }
            answerCheckboxContainer.addView(checkBox)
            if (option is OtherOption) {
                val otherView = OtherView(layoutInflater.inflate(R.layout.view_other_multi_select, answerCheckboxContainer, false))
                otherView.editText.inputType = if (option.type == "number") InputType.TYPE_CLASS_NUMBER else InputType.TYPE_CLASS_TEXT
                otherView.setVisibility(if (checkedTitles.contains(option.title)) View.VISIBLE else View.GONE)
                otherView.editText.setText(questionState.getString(getEditTextKey(option.title)))
                otherView.editText.imeOptions = EditorInfo.IME_ACTION_NEXT
                otherView.editText.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
                    if (actionId == EditorInfo.IME_ACTION_NEXT) {
                        onNext(questionState)
                        return@OnEditorActionListener true
                    }
                    false
                })
                otherView.editText.addTextChangedListener(editTextWatcher)
                answerCheckboxContainer.addView(otherView.otherSection)
                otherMap[checkBox.text.toString()] = otherView
            }
            checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
                val otherView = otherMap[buttonView.text.toString()]
                if (otherView != null) {
                    otherView.setVisibility(if (isChecked) View.VISIBLE else View.GONE)
                    if (isChecked) {
                        otherView.editText.requestFocus()
                        showKeyboard(context, otherView.editText)
                    }
                }
                if (isChecked) {
                    questionState.addStringToList(CHECKED_TITLES_KEY, buttonView.text.toString())
                } else {
                    questionState.removeStringFromList(CHECKED_TITLES_KEY, buttonView.text.toString())
                }
                //                    if (hasBeenAnswered(questionState)) {
//                        onNext(questionState);
//                    }
//                    onNext(questionState);
            }
        }
        nextButton.setOnClickListener { onNext(questionState) }
    }

    private fun getEditTextKey(title: String?): String {
        return String.format(EDIT_TEXT_KEY, title)
    }

    private val checkboxAnswers: ArrayList<String>
        private get() {
            val checkedTitles = ArrayList<String>()
            for (i in 0 until answerCheckboxContainer.childCount) {
                val child = answerCheckboxContainer.getChildAt(i)
                if (child !is CheckBox || !child.isChecked) {
                    continue
                }
                val checkboxTitle = child.text.toString()
                val otherView = otherMap[checkboxTitle]
                if (otherView != null) {
                    checkedTitles.add(otherView.editText.text.toString())
                } else {
                    checkedTitles.add(checkboxTitle)
                }
            }
            return checkedTitles
        }

    override fun resetState() {
        super.resetState()
        answerCheckboxContainer.removeAllViews()
        for (otherView in otherMap.values) {
            if (editTextWatcher != null) {
                otherView.editText.removeTextChangedListener(editTextWatcher)
            }
            otherView.editText.setOnEditorActionListener(null)
        }
        otherMap.clear()
        nextButton.setOnClickListener(null)
    }

    private fun onNext(questionState: QuestionState) {
        questionState.answer = Answer(checkboxAnswers)
        setHasBeenAnswered(questionState)
        hideKeyboard(context, answerCheckboxContainer)
    }

    // Returns true if the answer for this has been set before
    private fun hasBeenAnswered(questionState: QuestionState): Boolean {
        return questionState.getBool(HAS_BEEN_ANSWERED_KEY, false)
    }

    private fun setHasBeenAnswered(questionState: QuestionState) {
        questionState.put(HAS_BEEN_ANSWERED_KEY, true)
    }

    private inner class OtherView internal constructor(var otherSection: View) {
        var editText: EditText
        fun setVisibility(visibility: Int) {
            otherSection.visibility = visibility
        }

        init {
            editText = otherSection.findViewById(R.id.edit_other)
        }
    }

    companion object {
        private const val CHECKED_TITLES_KEY = "checked_titles"
        private const val HAS_BEEN_ANSWERED_KEY = "has_been_answered_key"
        private const val EDIT_TEXT_KEY = "%s_edit_text_key"
    }

    init {
        answerCheckboxContainer = itemView.findViewById(R.id.answer_checkbox_container)
        nextButton = itemView.findViewById(R.id.next_button)
    }
}