package com.questionnaire.questionnairandroid.viewholder

import android.content.Context
import android.view.View
import com.questionnaire.questionnairandroid.QuestionState
import com.questionnaire.questionnairandroid.question.DynamicLabelTextFieldQuestion

class DynamicLabelTextFieldQuestionViewHolder(context: Context, itemView: View) : QuestionViewHolder<DynamicLabelTextFieldQuestion?>(context, itemView) {
    fun bind(question: DynamicLabelTextFieldQuestion?, questionState: QuestionState?) {
        super.bind(question)
        // TODO
    }
}