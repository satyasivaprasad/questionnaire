package com.questionnaire.questionnairandroid.viewholder

import android.content.Context
import android.text.TextUtils
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.questionnaire.questionnairandroid.R
import com.questionnaire.questionnairandroid.question.Question

abstract class QuestionViewHolder<Q : Question?>(protected val context: Context, itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val headerText: TextView
    private val questionText: TextView

    fun bind(question: Q) {
        resetState()
        if (TextUtils.isEmpty(question!!.header)) {
            headerText.visibility = View.GONE
        } else {
            headerText.text = question.header
        }
        questionText.text = question.question
    }

    protected open fun resetState() {
        headerText.visibility = View.VISIBLE
    }

    init {
        headerText = itemView.findViewById(R.id.header_text)
        questionText = itemView.findViewById(R.id.question_text)
    }
}