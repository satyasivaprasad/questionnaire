package com.questionnaire.questionnairandroid.viewholder

import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView.OnEditorActionListener
import com.questionnaire.questionnairandroid.Answer
import com.questionnaire.questionnairandroid.QuestionState
import com.questionnaire.questionnairandroid.R
import com.questionnaire.questionnairandroid.question.OtherOption
import com.questionnaire.questionnairandroid.question.SingleSelectQuestion
import com.questionnaire.questionnairandroid.util.KeyboardUtil.hideKeyboard
import com.questionnaire.questionnairandroid.util.KeyboardUtil.showKeyboard
import com.questionnaire.questionnairandroid.util.SimpleTextWatcher
import com.questionnaire.questionnairandroid.util.TopAlignedRadioButton

class SingleSelectQuestionViewHolder(context: Context?, itemView: View) : QuestionViewHolder<SingleSelectQuestion?>(context!!, itemView) {
    private val answerSelector: RadioGroup
    private val otherSection: ViewGroup
    private val editOther: EditText
    private var editTextWatcher: TextWatcher? = null
    private val nextButton: Button
    fun bind(question: SingleSelectQuestion, questionState: QuestionState) {
        super.bind(question)
        var checkedId = -1
        for (option in question.options!!) {
            val radioButton: RadioButton = TopAlignedRadioButton(context)
            radioButton.text = option.title
            if (option is OtherOption) {
                radioButton.setTag(R.id.is_other, java.lang.Boolean.TRUE)
                radioButton.setTag(R.id.input_type_number, option.type == "number")
                if (shouldCheckButton(questionState, option.title)) {
                    otherSection.visibility = View.VISIBLE
                }
            }
            answerSelector.addView(radioButton)
            val extraLeftMargin = convertDpToPx(context, RADIO_BUTTON_EXTRA_LEFT_MARGIN_DP.toFloat()).toInt()
            val extraBottomMargin = convertDpToPx(context, RADIO_BUTTON_EXTRA_BOTTOM_MARGIN_DP.toFloat()).toInt()
            val params = radioButton.layoutParams as RadioGroup.LayoutParams
            params.setMargins(params.leftMargin + extraLeftMargin, params.topMargin, params.rightMargin, params.bottomMargin + extraBottomMargin)
            radioButton.layoutParams = params
            if (shouldCheckButton(questionState, option.title)) {
                checkedId = radioButton.id
            }
        }
        answerSelector.check(checkedId)
        answerSelector.setOnCheckedChangeListener { group, checkedId ->
            val selectedButton = answerSelector.findViewById<RadioButton>(checkedId)
            questionState.put(CHECKED_BUTTON_TITLE_KEY, selectedButton.text.toString())
            if (isButtonOther(selectedButton)) {
                editOther.inputType = if (isButtonTypeNumber(selectedButton)) InputType.TYPE_CLASS_NUMBER else InputType.TYPE_CLASS_TEXT
                otherSection.visibility = View.VISIBLE
                editOther.requestFocus()
                showKeyboard(context, editOther)
            } else {
                otherSection.visibility = View.GONE
                questionState.answer = Answer(selectedButton.text.toString())
                //                    setHasBeenAnswered(questionState);
                onNext(questionState)
            }
        }
        editOther.setText(questionState.getString(EDIT_TEXT_KEY))
        editTextWatcher = object : SimpleTextWatcher() {
            override fun afterTextChanged(s: Editable) {
                questionState.put(EDIT_TEXT_KEY, s.toString())
                if (hasBeenAnswered(questionState)) {
                    questionState.answer = Answer(s.toString())
                }
            }
        }
        editOther.addTextChangedListener(editTextWatcher)
        editOther.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                onNext(questionState)
                return@OnEditorActionListener true
            }
            false
        })
        nextButton.setOnClickListener { onNext(questionState) }
    }

    private fun shouldCheckButton(questionState: QuestionState, title: String?): Boolean {
        return title == questionState.getString(CHECKED_BUTTON_TITLE_KEY)
    }

    private fun isButtonOther(button: Button): Boolean {
        return button.getTag(R.id.is_other) != null && button.getTag(R.id.is_other) === java.lang.Boolean.TRUE
    }

    private fun isButtonTypeNumber(button: Button): Boolean {
        return button.getTag(R.id.input_type_number) != null && button.getTag(R.id.input_type_number) === java.lang.Boolean.TRUE
    }

    override fun resetState() {
        super.resetState()
        answerSelector.removeAllViews()
        answerSelector.setOnCheckedChangeListener(null)
        if (editTextWatcher != null) {
            editOther.removeTextChangedListener(editTextWatcher)
        }
        otherSection.visibility = View.GONE
        editOther.text = null
        editOther.setOnEditorActionListener(null)
        nextButton.setOnClickListener(null)
    }

    private fun onNext(questionState: QuestionState) {
        val selectedButton = answerSelector.findViewById<RadioButton>(answerSelector.checkedRadioButtonId)
        if (isButtonOther(selectedButton)) {
            questionState.answer = Answer(editOther.text.toString())
            editOther.clearFocus()
            hideKeyboard(context, editOther)
        }
        setHasBeenAnswered(questionState)
    }

    // Returns true if the answer for this has been set before
    private fun hasBeenAnswered(questionState: QuestionState): Boolean {
        return questionState.getBool(HAS_BEEN_ANSWERED_KEY, false)
    }

    private fun setHasBeenAnswered(questionState: QuestionState) {
        questionState.put(HAS_BEEN_ANSWERED_KEY, true)
    }

    fun convertDpToPx(context: Context, dp: Float): Float {
        return dp * context.resources.displayMetrics.density
    }

    companion object {
        private const val CHECKED_BUTTON_TITLE_KEY = "checked_button_title"
        private const val EDIT_TEXT_KEY = "edit_text"
        private const val HAS_BEEN_ANSWERED_KEY = "has_been_answered_key"
        private const val RADIO_BUTTON_EXTRA_LEFT_MARGIN_DP = 2
        private const val RADIO_BUTTON_EXTRA_BOTTOM_MARGIN_DP = 7
    }

    init {
        answerSelector = itemView.findViewById(R.id.answer_selector)
        otherSection = itemView.findViewById(R.id.other_section)
        editOther = itemView.findViewById(R.id.edit_other)
        nextButton = itemView.findViewById(R.id.next_button)
    }
}