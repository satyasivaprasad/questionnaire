package com.questionnaire.questionnairandroid.viewholder

import android.content.Context
import android.view.View
import com.questionnaire.questionnairandroid.QuestionState
import com.questionnaire.questionnairandroid.question.DatePickerQuestion

class DatePickerQuestionViewHolder(context: Context, itemView: View) : QuestionViewHolder<DatePickerQuestion?>(context, itemView) {
    fun bind(question: DatePickerQuestion?, questionState: QuestionState?) {
        super.bind(question)
        // TODO
    }
}