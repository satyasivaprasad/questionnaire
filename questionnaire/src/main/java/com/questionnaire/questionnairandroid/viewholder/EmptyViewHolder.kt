package com.questionnaire.questionnairandroid.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class EmptyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)