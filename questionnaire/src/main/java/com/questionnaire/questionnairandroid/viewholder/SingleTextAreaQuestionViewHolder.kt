package com.questionnaire.questionnairandroid.viewholder

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.questionnaire.questionnairandroid.Answer
import com.questionnaire.questionnairandroid.AnswerProvider
import com.questionnaire.questionnairandroid.QuestionState
import com.questionnaire.questionnairandroid.R
import com.questionnaire.questionnairandroid.question.SingleTextAreaQuestion
import com.questionnaire.questionnairandroid.question.Validation
import com.questionnaire.questionnairandroid.util.KeyboardUtil.hideKeyboard
import com.questionnaire.questionnairandroid.util.SimpleTextWatcher
import com.questionnaire.questionnairandroid.validation.ValidationResult.Companion.success
import com.questionnaire.questionnairandroid.validation.Validator
import java.util.*

class SingleTextAreaQuestionViewHolder(context: Context?, itemView: View, private val mValidator: Validator?, private val mAnswerProvider: AnswerProvider) : QuestionViewHolder<SingleTextAreaQuestion?>(context!!, itemView) {
    private val answerInputLayout: TextInputLayout
    private val answerEdit: TextInputEditText
    private var editTextWatcher: TextWatcher? = null
    private val nextButton: Button
    private val validator: Validator?
        private get() = mValidator

    fun bind(singleTextAreaQuestion: SingleTextAreaQuestion, questionState: QuestionState) {
        super.bind(singleTextAreaQuestion)
        if (singleTextAreaQuestion.maxChars != null) {
            answerInputLayout.isCounterEnabled = true
            answerInputLayout.counterMaxLength = Integer.valueOf(singleTextAreaQuestion.maxChars)
        }
        answerEdit.setText(questionState.getString(EDIT_TEXT_KEY))
        editTextWatcher = object : SimpleTextWatcher() {
            override fun afterTextChanged(s: Editable) {
                questionState.put(EDIT_TEXT_KEY, s.toString())
                //                if (hasBeenAnswered(questionState)) {
//                    questionState.setAnswer(new Answer(s.toString()));
//                }
                questionState.answer = Answer(s.toString())
            }
        }
        answerEdit.addTextChangedListener(editTextWatcher)
        nextButton.setOnClickListener { onNext(questionState, singleTextAreaQuestion.validations) }
        if (!hasBeenAnswered(questionState)) {
//            answerEdit.requestFocus();
//            KeyboardUtil.showKeyboardDelayed(getContext(), answerEdit, 500L);
        }
    }

    override fun resetState() {
        super.resetState()
        if (editTextWatcher != null) {
            answerEdit.removeTextChangedListener(editTextWatcher)
        }
        answerEdit.text = null
        answerInputLayout.isCounterEnabled = false
        nextButton.setOnClickListener(null)
    }

    private fun onNext(questionState: QuestionState, validations: ArrayList<Validation>?) {
        val answerStr = if (answerEdit.text != null) answerEdit.text.toString() else null
        check(!(mValidator == null && validations != null && !validations.isEmpty())) { "No validator available for validations" }
        val validationResult = mValidator?.validate(validations!!, answerStr!!, mAnswerProvider)
                ?: success()
        if (validationResult.isValid) {
            questionState.answer = Answer(answerStr!!)
            setHasBeenAnswered(questionState)
            answerEdit.clearFocus()
            hideKeyboard(context, answerEdit)
        } else {
            mValidator?.validationFailed(validationResult.failedMessage)
            //            answerEdit.requestFocus();
//            KeyboardUtil.showKeyboard(getContext(), answerEdit);
        }
    }

    // Returns true if the answer for this has been set before
    private fun hasBeenAnswered(questionState: QuestionState): Boolean {
        return questionState.getBool(HAS_BEEN_ANSWERED_KEY, false)
    }

    private fun setHasBeenAnswered(questionState: QuestionState) {
        questionState.put(HAS_BEEN_ANSWERED_KEY, true)
    }

    companion object {
        private const val EDIT_TEXT_KEY = "edit_text"
        private const val HAS_BEEN_ANSWERED_KEY = "has_been_answered_key"
    }

    init {
        answerInputLayout = itemView.findViewById(R.id.answer_input_layout)
        answerEdit = itemView.findViewById(R.id.answer_edit)
        nextButton = itemView.findViewById(R.id.next_button)
    }
}