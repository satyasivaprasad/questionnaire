package com.questionnaire.questionnairandroid.viewholder

import android.view.View
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.questionnaire.questionnairandroid.AnswerProvider
import com.questionnaire.questionnairandroid.R
import com.questionnaire.questionnairandroid.SubmitQuestionnaireHandler
import com.questionnaire.questionnairandroid.question.QuestionsWrapper.SubmitData

class SubmitViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val submitButton: Button
    fun bind(submitData: SubmitData?, answerProvider: AnswerProvider, submitQuestionnaireHandler: SubmitQuestionnaireHandler?) {
        submitButton.text = submitData?.buttonTitle
        submitButton.setOnClickListener { submitQuestionnaireHandler?.submit(submitData?.url, answerProvider.allAnswersJson()) }
    }

    init {
        submitButton = itemView.findViewById(R.id.submit_button)
    }
}