package com.questionnaire.questionnairandroid.viewholder

import android.content.Context
import android.view.View
import com.questionnaire.questionnairandroid.QuestionState
import com.questionnaire.questionnairandroid.question.TableSelectQuestion

class TableSelectQuestionViewHolder(context: Context?, itemView: View) : QuestionViewHolder<TableSelectQuestion?>(context!!, itemView) {
    fun bind(question: TableSelectQuestion?, questionState: QuestionState?) {
        super.bind(question)
        // TODO
    }
}