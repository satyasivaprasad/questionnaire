package com.questionnaire.questionnairandroid.viewholder

import android.content.Context
import android.view.View
import com.questionnaire.questionnairandroid.QuestionState
import com.questionnaire.questionnairandroid.question.AddTextFieldQuestion

class AddTextFieldQuestionViewHolder(context: Context, itemView: View) : QuestionViewHolder<AddTextFieldQuestion?>(context, itemView) {
    fun bind(question: AddTextFieldQuestion?, questionState: QuestionState?) {
        super.bind(question)
        // TODO
    }
}