package com.questionnaire.questionnairandroid

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.questionnaire.questionnairandroid.question.*
import com.questionnaire.questionnairandroid.viewholder.*

class QuestionnaireAdapter(private val context: Context, private val mState: QuestionnaireState) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    enum class QuestionType(private val mValue: String) {
        SINGLE_SELECT("single_select"), MULTI_SELECT("multi_select"), YEAR_PICKER("year_picker"), DATE_PICKER("date_picker"), SINGLE_TEXT_FIELD("single_text_field"), SINGLE_TEXT_AREA("single_text_area"), MULTI_TEXT_FIELD("multi_text_field"), DYNAMIC_LABEL_TEXT_FIELD("dynamic_label_text_field"), ADD_TEXT_FIELD("add_text_field"), SEGMENT_SELECT("segment_select"), TABLE_SELECT("table_select"), SUBMIT("submit"), EMPTY("empty"), UNSUPPORTED("");

        override fun toString(): String {
            return mValue
        }

        companion object {
            fun fromString(value: String?): QuestionType? {
                for (questionType in values()) {
                    if (questionType.mValue == value) {
                        return questionType
                    }
                }
                return null
            }
        }

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val questionType = QuestionType.values()[viewType]
        val view = LayoutInflater.from(context).inflate(getLayout(questionType), viewGroup, false)
        return when (questionType) {
            QuestionType.SINGLE_SELECT -> SingleSelectQuestionViewHolder(context, view)
            QuestionType.MULTI_SELECT -> MultiSelectQuestionViewHolder(context, view)
            QuestionType.YEAR_PICKER -> YearPickerQuestionViewHolder(context, view)
            QuestionType.DATE_PICKER -> DatePickerQuestionViewHolder(context, view)
            QuestionType.SINGLE_TEXT_FIELD -> SingleTextFieldQuestionViewHolder(context, view, mState.validator, mState)
            QuestionType.SINGLE_TEXT_AREA -> SingleTextAreaQuestionViewHolder(context, view, mState.validator, mState)
            QuestionType.MULTI_TEXT_FIELD -> MultiTextFieldQuestionViewHolder(context, view)
            QuestionType.DYNAMIC_LABEL_TEXT_FIELD -> DynamicLabelTextFieldQuestionViewHolder(context, view)
            QuestionType.ADD_TEXT_FIELD -> AddTextFieldQuestionViewHolder(context, view)
            QuestionType.SEGMENT_SELECT -> SegmentSelectQuestionViewHolder(context, view)
            QuestionType.TABLE_SELECT -> TableSelectQuestionViewHolder(context, view)
            QuestionType.SUBMIT -> SubmitViewHolder(view)
            QuestionType.EMPTY, QuestionType.UNSUPPORTED -> EmptyViewHolder(view)
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        var question: Question? = null
        val questionType: QuestionType?
        var questionState: QuestionState? = null
        if (mState.isSubmitPosition(position)) {
            questionType = QuestionType.UNSUPPORTED
        } else {
            question = getItem(position)
            if (question == null) {
                Log.e(TAG, "Unable to find question for position: $position")
                return
            }
            questionType = getQuestionType(position)
            questionState = mState.getStateFor(question.id)
        }
        when (questionType) {
            QuestionType.SINGLE_SELECT -> if (viewHolder is SingleSelectQuestionViewHolder && question is SingleSelectQuestion) {
                viewHolder.bind((question as SingleSelectQuestion?)!!, questionState!!)
            }
            QuestionType.MULTI_SELECT -> if (viewHolder is MultiSelectQuestionViewHolder && question is MultiSelectQuestion) {
                viewHolder.bind((question as MultiSelectQuestion?)!!, questionState!!)
            }
            QuestionType.YEAR_PICKER -> if (viewHolder is YearPickerQuestionViewHolder && question is YearPickerQuestion) {
                viewHolder.bind(question as YearPickerQuestion?, questionState)
            }
            QuestionType.DATE_PICKER -> if (viewHolder is DatePickerQuestionViewHolder && question is DatePickerQuestion) {
                viewHolder.bind(question as DatePickerQuestion?, questionState)
            }
            QuestionType.SINGLE_TEXT_FIELD -> if (viewHolder is SingleTextFieldQuestionViewHolder && question is SingleTextFieldQuestion) {
                viewHolder.bind((question as SingleTextFieldQuestion?)!!, questionState!!)
            }
            QuestionType.SINGLE_TEXT_AREA -> {
                if (viewHolder is SingleTextAreaQuestionViewHolder && question is SingleTextAreaQuestion) {
                    viewHolder.bind((question as SingleTextAreaQuestion?)!!, questionState!!)
                }
                if (viewHolder is MultiTextFieldQuestionViewHolder && question is MultiTextFieldQuestion) {
                    viewHolder.bind(question as MultiTextFieldQuestion?, questionState)
                }
            }
            QuestionType.MULTI_TEXT_FIELD -> if (viewHolder is MultiTextFieldQuestionViewHolder && question is MultiTextFieldQuestion) {
                viewHolder.bind(question as MultiTextFieldQuestion?, questionState)
            }
            QuestionType.DYNAMIC_LABEL_TEXT_FIELD -> if (viewHolder is DynamicLabelTextFieldQuestionViewHolder && question is DynamicLabelTextFieldQuestion) {
                viewHolder.bind(question as DynamicLabelTextFieldQuestion?, questionState)
            }
            QuestionType.ADD_TEXT_FIELD -> if (viewHolder is AddTextFieldQuestionViewHolder && question is AddTextFieldQuestion) {
                viewHolder.bind(question as AddTextFieldQuestion?, questionState)
            }
            QuestionType.SEGMENT_SELECT -> if (viewHolder is SegmentSelectQuestionViewHolder && question is SegmentSelectQuestion) {
                viewHolder.bind((question as SegmentSelectQuestion?)!!, questionState!!)
            }
            QuestionType.TABLE_SELECT -> if (viewHolder is TableSelectQuestionViewHolder && question is TableSelectQuestion) {
                viewHolder.bind(question as TableSelectQuestion?, questionState)
            }
            QuestionType.SUBMIT -> if (viewHolder is SubmitViewHolder) {
                viewHolder.bind(mState.submitData, mState, mState.submitQuestionnaireHandler)
            }
            QuestionType.EMPTY, QuestionType.UNSUPPORTED -> {
            }
        }
    }

    @LayoutRes
    private fun getLayout(questionType: QuestionType): Int {
        return when (questionType) {
            QuestionType.SINGLE_SELECT -> R.layout.view_single_select_question
            QuestionType.MULTI_SELECT -> R.layout.view_multi_select_question
            QuestionType.YEAR_PICKER -> R.layout.view_year_picker_question
            QuestionType.DATE_PICKER -> R.layout.view_date_picker_question
            QuestionType.SINGLE_TEXT_FIELD -> R.layout.view_single_text_field_question
            QuestionType.SINGLE_TEXT_AREA -> R.layout.view_single_text_area_question
            QuestionType.MULTI_TEXT_FIELD -> R.layout.view_multi_text_field_question
            QuestionType.DYNAMIC_LABEL_TEXT_FIELD -> R.layout.view_dynamic_label_text_field_question
            QuestionType.ADD_TEXT_FIELD -> R.layout.view_add_text_field_question
            QuestionType.SEGMENT_SELECT -> R.layout.view_segment_select_question
            QuestionType.TABLE_SELECT -> R.layout.view_table_select_question
            QuestionType.SUBMIT -> R.layout.view_submit_button
            QuestionType.EMPTY, QuestionType.UNSUPPORTED -> R.layout.view_empty
        }
        return 0
    }

    override fun getItemViewType(position: Int): Int {
        return getQuestionType(position)!!.ordinal
    }

    private fun getItem(position: Int): Question? {
        return mState.getQuestionFor(position)
    }

    private fun getQuestionType(position: Int): QuestionType? {
        if (mState.isSubmitPosition(position)) {
            return QuestionType.SUBMIT
        }
        val question = getItem(position)
        if (question == null) {
            Log.e(TAG, "Unable to find question for position: $position, using EMPTY")
            return QuestionType.EMPTY
        }
        return QuestionType.fromString(question.questionType)
    }

    override fun getItemCount(): Int {
        return mState.getVisibleQuestionCount() + if (mState.isSubmitButtonShown) 1 else 0
    }

    companion object {
        private val TAG = QuestionnaireAdapter::class.java.simpleName
    }

}