package com.questionnaire.questionnairandroid.util

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

/**
 * Utility to hide a keyboard.
 */
object KeyboardUtil {
    @JvmStatic
    fun showKeyboard(context: Context, view: View?) {
        val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (view != null && inputMethodManager != null) {
            inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    @JvmStatic
    fun hideKeyboard(context: Context, view: View?) {
        val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (view != null && inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun showKeyboardDelayed(context: Context, view: View, delay: Long) {
        view.postDelayed({ showKeyboard(context, view) }, delay)
    }
}