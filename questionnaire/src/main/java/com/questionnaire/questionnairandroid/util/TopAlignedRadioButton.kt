package com.questionnaire.questionnairandroid.util

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatRadioButton

class TopAlignedRadioButton : AppCompatRadioButton {
    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {}
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun onDraw(canvas: Canvas) {
        var buttonDrawable: Drawable? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            buttonDrawable = getButtonDrawable()
        }
        if (buttonDrawable == null) {
            super.onDraw(canvas)
            return
        }
        val drawableHeight = buttonDrawable.intrinsicHeight
        val drawableWidth = buttonDrawable.intrinsicWidth
        var top = 0
        if (lineCount > 1) {
            top = convertDpToPx(context, MULTI_LINE_DRAWABLE_VERTICAL_CORRECTION_DP).toInt()
        }
        val bottom = top + drawableHeight
        val left = convertDpToPx(context, EXTRA_LEFT_PADDING_DP.toFloat()).toInt()
        val right = left + drawableWidth
        buttonDrawable.setBounds(left, top, right, bottom)
        buttonDrawable.draw(canvas)

        // We don't want CompoundButton to redraw the same drawable, but if we set it to null
        // the TextView won't draw in the proper place.  So we copy and set it to transparent.
        val buttonDrawableCopy = buttonDrawable.constantState.newDrawable().mutate()
        buttonDrawableCopy.alpha = 0
        setButtonDrawable(buttonDrawableCopy)
        super.onDraw(canvas)
        setButtonDrawable(buttonDrawable)
        val background = background
        if (background != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                background.setHotspotBounds(left, top, right, bottom)
            }
        }
    }

    fun convertDpToPx(context: Context, dp: Float): Float {
        return dp * context.resources.displayMetrics.density
    }

    companion object {
        private const val EXTRA_LEFT_PADDING_DP = -2
        private const val MULTI_LINE_DRAWABLE_VERTICAL_CORRECTION_DP = -5.71428f
    }
}