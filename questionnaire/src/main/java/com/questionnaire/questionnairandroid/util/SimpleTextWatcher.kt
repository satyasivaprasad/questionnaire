package com.questionnaire.questionnairandroid.util

import android.text.Editable
import android.text.TextWatcher

// Empty impl to make classes that need only implement one of these methods cleaner.
open class SimpleTextWatcher : TextWatcher {
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    override fun afterTextChanged(s: Editable) {}
}