package com.questionnaire.questionnairandroid.validation

interface FailedValidationListener {
    fun validationFailed(message: String?)
}