package com.questionnaire.questionnairandroid.validation

import com.questionnaire.questionnairandroid.AnswerProvider
import com.questionnaire.questionnairandroid.question.Validation

interface Validator {
    fun validate(validations: List<Validation>, answer: String, answerProvider: AnswerProvider): ValidationResult
    fun validate(validations: List<Validation>, answer: Map<String, String>, answerProvider: AnswerProvider): ValidationResult
    fun validationFailed(message: String)
}