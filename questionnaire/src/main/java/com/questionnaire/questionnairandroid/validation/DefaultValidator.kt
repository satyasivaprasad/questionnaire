package com.questionnaire.questionnairandroid.validation

import android.text.TextUtils
import com.questionnaire.questionnairandroid.AnswerProvider
import com.questionnaire.questionnairandroid.question.Validation

class DefaultValidator(private val mListener: FailedValidationListener?) : Validator {
    override fun validate(validations: List<Validation>, answer: String, answerProvider: AnswerProvider): ValidationResult {
        if (validations == null || validations.isEmpty()) {
            return ValidationResult.success()
        }
        for (validation in validations) {
            if (!isConditionMet(validation, answer, answerProvider)) {
                return ValidationResult(false, validation.onFailMessage!!)
            }
        }
        return ValidationResult.success()
    }

    override fun validate(validations: List<Validation>, answers: Map<String, String>, answerProvider: AnswerProvider): ValidationResult {
        if (validations == null || validations.isEmpty()) {
            return ValidationResult.success()
        }
        for (validation in validations) {
            val answer = answers[validation.forLabel] ?: continue
            if (!isConditionMet(validation, answer, answerProvider)) {
                return ValidationResult(false, validation.onFailMessage!!)
            }
        }
        return ValidationResult.success()
    }

    private fun isConditionMet(validation: Validation, answer: String, answerProvider: AnswerProvider?): Boolean {
        var value = validation.value
        if (validation.answerToQuestionId != null) {
            checkNotNull(answerProvider) { "Validation requires a non-null AnswerProvider" }
            value = answerProvider.answerFor(validation.answerToQuestionId!!).value?.toDouble()
        }
        if (TextUtils.isEmpty(answer)) {
            return false
        }
        val numAnswer: Double
        numAnswer = try {
            answer.toDouble()
        } catch (nfe: NumberFormatException) {
            return false
        }
        when (validation.operation) {
            "equals" -> return numAnswer == value
            "not equals" -> return numAnswer != value
            "greater than" -> return numAnswer.compareTo(value!!) > 0
            "greater than or equal to" -> return numAnswer.compareTo(value!!) >= 0
            "less than" -> return numAnswer.compareTo(value!!) < 0
            "less than or equal to" -> return numAnswer.compareTo(value!!) <= 0
        }
        return false
    }

    override fun validationFailed(message: String) {
        mListener?.validationFailed(message)
    }

}