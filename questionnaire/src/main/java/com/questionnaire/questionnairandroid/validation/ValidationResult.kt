package com.questionnaire.questionnairandroid.validation

class ValidationResult(var isValid: Boolean, var failedMessage: String) {

    companion object {
        @JvmStatic
        fun success(): ValidationResult {
            return ValidationResult(true, "")
        }
    }

}