package com.questionnaire.questionnairandroid

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.android.volley.Response
import org.json.JSONException
import org.json.JSONObject

class DefaultSubmitQuestionnaireHandler(private val context: Context) : SubmitQuestionnaireHandler {
    private var responseListener: Response.Listener<JSONObject>
    private var errorListener: Response.ErrorListener

    fun setResponseListener(responseListener: Response.Listener<JSONObject>) {
        this.responseListener = responseListener
    }

    fun setErrorListener(errorListener: Response.ErrorListener) {
        this.errorListener = errorListener
    }

    override fun submit(url: String?, jsonQuestionAnswerData: String?) {
        var requestBody: JSONObject? = null
        requestBody = try {
            JSONObject(jsonQuestionAnswerData)
        } catch (e: JSONException) {
            e.printStackTrace()
            return
        }
        Log.v(LOG_TAG, "jsonQuestionAnswerData $jsonQuestionAnswerData")
    }

    companion object {
        private val LOG_TAG = DefaultSubmitQuestionnaireHandler::class.java.simpleName
    }

    init {
        responseListener = Response.Listener { response -> Toast.makeText(context, "Response: $response", Toast.LENGTH_LONG).show() }
        errorListener = Response.ErrorListener { error -> Toast.makeText(context, "ERROR: $error", Toast.LENGTH_LONG).show() }
    }
}