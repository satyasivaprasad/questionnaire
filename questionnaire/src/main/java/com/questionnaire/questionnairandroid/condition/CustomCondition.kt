package com.questionnaire.questionnairandroid.condition

import java.util.*

class CustomCondition : Condition() {
    var ids: ArrayList<String>? = null
    var extra: HashMap<String?, String?>? = null
    override fun toString(): String {
        val stringBuilder = StringBuilder("CustomCondition: Evaluating ids: ")
        for (id in ids!!) {
            stringBuilder.append(id)
                    .append("\n")
        }
        stringBuilder.append("for operation: ")
                .append(operation)
                .append(":\n")
        stringBuilder.append("With extras:\n")
        for ((key, value) in extra!!) {
            stringBuilder.append(key)
                    .append(" : ")
                    .append(value)
                    .append("\n")
        }
        return stringBuilder.toString()
    }
}