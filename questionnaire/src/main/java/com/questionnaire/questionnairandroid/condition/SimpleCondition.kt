package com.questionnaire.questionnairandroid.condition

class SimpleCondition : Condition() {
    var id: String? = null
    var subid: String? = null
    var value: String? = null
    override fun toString(): String {
        return "SimpleCondition: id = $id, subid = $subid, operation = $operation, value = $value"
    }
}