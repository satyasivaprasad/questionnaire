package com.questionnaire.questionnairandroid.condition

import java.util.*

class DecisionCondition : Condition() {
    var subconditions: ArrayList<Condition>? = null
    override fun toString(): String {
        val stringBuilder = StringBuilder("DecisionCondition: Evaluating subconditions for operation: ")
                .append(operation)
                .append(":\n")
        for (condition in subconditions!!) {
            stringBuilder.append(condition)
                    .append("\n")
        }
        return stringBuilder.toString()
    }
}