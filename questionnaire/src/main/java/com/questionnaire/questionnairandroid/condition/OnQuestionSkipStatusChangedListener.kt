package com.questionnaire.questionnairandroid.condition

import com.questionnaire.questionnairandroid.condition.FilteredQuestions.QuestionAdapterPosition

interface OnQuestionSkipStatusChangedListener {
    fun skipStatusChanged(newlySkippedQuestionIds: Set<QuestionAdapterPosition?>?, newlyShownQuestionIds: Set<QuestionAdapterPosition?>?)
}