package com.questionnaire.questionnairandroid.condition

import com.questionnaire.questionnairandroid.OnQuestionStateChangedListener
import com.questionnaire.questionnairandroid.QuestionState
import com.questionnaire.questionnairandroid.Questions
import com.questionnaire.questionnairandroid.question.Question
import java.util.*

class FilteredQuestions(private val questions: Questions, private val conditionEvaluator: ConditionEvaluator) : OnQuestionStateChangedListener {
    private var adapterToRealPositions: ArrayList<Int>? = null
    private var skippedQuestions: Set<QuestionAdapterPosition> = HashSet()
    private var skipStatusChangedListener: OnQuestionSkipStatusChangedListener? = null
    fun setOnQuestionSkipStatusChangedListener(listener: OnQuestionSkipStatusChangedListener?) {
        skipStatusChangedListener = listener
    }

    fun getQuestionFor(adapterPosition: Int): Question? {
        val realIndex = adapterToReal(adapterPosition)
        return if (realIndex == null) null else questions.getQuestionFor(realIndex)
    }

    private fun adapterToReal(adapterPosition: Int): Int? {
        return if (adapterPosition < 0 || adapterPosition > adapterToRealPositions!!.size) {
            null
        } else adapterToRealPositions!![adapterPosition]
    }

    fun size(): Int {
        return adapterToRealPositions!!.size
    }

    private fun isSkipped(question: Question): Boolean {
        return !conditionEvaluator.isConditionMet(question.showIf)
    }

    private fun updateAdapterToRealPositions() {
        val skippedQuestions: MutableSet<QuestionAdapterPosition> = HashSet()
        val oldSkippedQuestions = this.skippedQuestions
        val newlySkipped: MutableSet<QuestionAdapterPosition> = HashSet()
        val newlyShown: MutableSet<QuestionAdapterPosition> = HashSet()
        adapterToRealPositions = ArrayList()
        for (i in 0 until questions.size()) {
            val question = questions.getQuestionFor(i)
            if (question != null && isSkipped(question)) {
                val skippedInfo = QuestionAdapterPosition(question.id!!, adapterToRealPositions!!.size)
                skippedQuestions.add(skippedInfo)
                if (!oldSkippedQuestions.contains(skippedInfo)) {
                    newlySkipped.add(skippedInfo)
                }
                continue
            } else {
                val positionInfo = QuestionAdapterPosition(question!!.id!!, adapterToRealPositions!!.size)
                if (oldSkippedQuestions.contains(positionInfo)) {
                    newlyShown.add(positionInfo)
                }
            }
            adapterToRealPositions!!.add(i)
        }
        if (skipStatusChangedListener == null) {
            return
        }
        this.skippedQuestions = skippedQuestions
        skipStatusChangedListener!!.skipStatusChanged(newlySkipped, newlyShown)
    }

    override fun questionStateChanged(newQuestionState: QuestionState) {}
    override fun questionAnswered(newQuestionState: QuestionState) {
        updateAdapterToRealPositions()
    }

    inner class QuestionAdapterPosition(var questionId: String, // If skipped, this will be the next adapter position
                                        var adapterPosition: Int) {

        override fun hashCode(): Int {
            return questionId.hashCode()
        }

        override fun equals(obj: Any?): Boolean {
            if (obj == null) {
                return false
            }
            return if (obj !is QuestionAdapterPosition) {
                false
            } else questionId == obj.questionId
        }

    }

    init {
        updateAdapterToRealPositions()
    }
}