package com.questionnaire.questionnairandroid.condition

import com.questionnaire.questionnairandroid.Answer

interface CustomConditionHandler {
    fun isConditionMet(answers: MutableMap<String, Answer>, extra: Map<String?, String?>?): Boolean
}