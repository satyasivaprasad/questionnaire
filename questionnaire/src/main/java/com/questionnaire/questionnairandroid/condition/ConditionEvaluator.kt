package com.questionnaire.questionnairandroid.condition

import android.util.Log
import com.questionnaire.questionnairandroid.Answer
import com.questionnaire.questionnairandroid.AnswerProvider
import java.util.*

class ConditionEvaluator(private val answerProvider: AnswerProvider) {
    private var customConditionHandler: CustomConditionHandler? = null
    fun setCustomConditionHandler(handler: CustomConditionHandler?) {
        customConditionHandler = handler
    }

    fun isConditionMet(condition: Condition?): Boolean {
        var result = false
        if (condition == null) {
            result = true
        } else if (condition is SimpleCondition) {
            result = isConditionMet(condition)
        } else if (condition is DecisionCondition) {
            result = isConditionMet(condition)
        } else if (condition is CustomCondition) {
            result = isConditionMet(condition)
        }
        Log.d(TAG, "Evaluated Condition: $condition as $result")
        return result
    }

    private fun isConditionMet(simpleCondition: SimpleCondition): Boolean {
        var answer = simpleCondition.id?.let { answerProvider.answerFor(it) }
        if (simpleCondition.subid != null && answer != null) {
            answer = if (answer.valueMap == null) null else answer.valueMap!![simpleCondition.subid!!]
        }
        // Empty answer reasults depend on the operation
        if (answer == null) {
            when (simpleCondition.operation) {
                "equals" -> return simpleCondition.value == null
                "not equals" -> return simpleCondition.value != null
                "greater than", "greater than or equal to", "less than", "less than or equal to" -> return false
                "contains" -> return false
                "not contains" -> return true
            }
        }
        when (simpleCondition.operation) {
            "equals" -> return simpleCondition.value == answer.value
            "not equals" -> return simpleCondition.value != answer.value
            "greater than" -> {
                val doubleValue = simpleCondition.value?.toDouble()
                val doubleAnswer = answer.value?.toDouble()
                return doubleValue?.let { doubleAnswer?.compareTo(it) }!! > 0
            }
            "greater than or equal to" -> {
                val doubleValue = simpleCondition.value?.toDouble()
                val doubleAnswer = answer.value?.toDouble()
                return doubleValue?.let { doubleAnswer?.compareTo(it) }!! >= 0
            }
            "less than" -> {
                val doubleValue = simpleCondition.value?.toDouble()
                val doubleAnswer = answer.value?.toDouble()
                return doubleValue?.let { doubleAnswer?.compareTo(it) }!! < 0
            }
            "less than or equal to" -> {
                val doubleValue = simpleCondition.value?.toDouble()
                val doubleAnswer = answer.value?.toDouble()
                return doubleValue?.let { doubleAnswer?.compareTo(it) }!! <= 0
            }
            "contains" -> return answer.valueList != null && answer.valueList!!.contains(simpleCondition.value)
            "not contains" -> return answer.valueList != null && !answer.valueList!!.contains(simpleCondition.value)
        }
        return false
    }

    private fun isConditionMet(decisionCondition: DecisionCondition): Boolean {
        for (condition in decisionCondition.subconditions!!) {
            if (decisionCondition.operation == "or" && isConditionMet(condition)) {
                return true
            } else if (decisionCondition.operation == "and" && !isConditionMet(condition)) {
                return false
            }
        }
        return decisionCondition.operation == "and"
    }

    private fun isConditionMet(customCondition: CustomCondition): Boolean {
        checkNotNull(customConditionHandler) { "CustomConditionHandler must be set!" }
        val answerMap: MutableMap<String, Answer> = HashMap()
        for (questionId in customCondition.ids!!) {
            answerMap[questionId] = answerProvider.answerFor(questionId)
        }
        return customConditionHandler!!.isConditionMet(answerMap, customCondition.extra)
    }

    companion object {
        private val TAG = ConditionEvaluator::class.java.simpleName
    }

}