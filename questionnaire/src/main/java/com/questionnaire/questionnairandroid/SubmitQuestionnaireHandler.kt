package com.questionnaire.questionnairandroid

interface SubmitQuestionnaireHandler {
    fun submit(url: String?, jsonQuestionAnswerData: String?)
}